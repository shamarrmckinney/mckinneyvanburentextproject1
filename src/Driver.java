
import java.io.File;
import java.io.IOException;
import java.util.Scanner;


/**
 * This is a main class and acts as a 'driver' to the connected 'Functions' class
 * Functions class is doing all the work
 * @author Shamarr McKinney-VanBuren
 */
public class Driver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        
        //A new function object
        Functions testFunct = new Functions();
        
        //Run to command Line
        String fileName = args[0];
        
        testFunct.addWords(fileName);
        
       
        
       /** Print to Console
       Scanner scan = new Scanner(System.in);
       
       String fileName1 = scan.nextLine();
            
       testFunct.addWords(fileName1);
        
       **/
        
       //EXTRA TEST CASES 
       //testFunct.addWords("test.txt"); 
       //testFunct.addWords(test2.txt);
       //testFunct.addWords(test3.txt);


        
    }
    
}
