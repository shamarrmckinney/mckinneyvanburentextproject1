
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * This class creates a GUI window which allows a user to pick or enter a text file
 * Then, displays the words, their word count, and 5 top & bottom used words
 * @author Shamarr McKinney-VanBuren
 */
public class GuiWindow extends JFrame implements ActionListener{
    
    private JTextField fileName = new JTextField();
    private JButton countWordsButton = new JButton( "Count it!");
    private JButton fileButton = new JButton("Pick a File!");
    private JTextArea fullAnswer = new JTextArea();
    private JTextArea topFiveAnswer = new JTextArea();
    private JTextArea bottomFiveAnswer = new JTextArea();
    private JLabel fileNameLabel = new JLabel( "File Name (end in .txt)");
    private JLabel outputLabel = new JLabel( "Output");
    private JLabel topFiveLabel = new JLabel( "Top 5 Used Words");
    private JLabel bottomFiveLabel = new JLabel( "Bottom 5 Used Words");
    private JScrollPane fullAnsScroll =  new JScrollPane( fullAnswer);
    private JScrollPane topFiveScroll =  new JScrollPane( topFiveAnswer);
    private JScrollPane bottomFiveScroll =  new JScrollPane( bottomFiveAnswer);


    
    /**
     * Creating a GUI window for the program and setting components
     */
    public GuiWindow() {
        this.setTitle("Word Count IT!");
        this.setBounds(400, 0, 700, 700);
        this.getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.fileName.setBounds( 300, 20, 150, 30);
        this.getContentPane().add( fileName);
                
        this.fileNameLabel.setBounds( 150, 20, 150, 30); 
        this.getContentPane().add( fileNameLabel);
        
        this.outputLabel.setBounds( 140, 170, 100, 30); 
        this.getContentPane().add( outputLabel);
        
        this.topFiveLabel.setBounds( 320, 170, 150, 30); 
        this.getContentPane().add( topFiveLabel);

        this.bottomFiveLabel.setBounds( 510, 170, 150, 30); 
        this.getContentPane().add( bottomFiveLabel);

        fullAnswer.setEditable(false);
        this.fullAnsScroll.setBounds(65, 200, 200, 300);
        this.getContentPane().add( fullAnsScroll);
        
        topFiveAnswer.setEditable(false);
        this.topFiveScroll.setBounds(300, 200, 150, 150);
        this.getContentPane().add(topFiveScroll);
        
        bottomFiveAnswer.setEditable(false);
        this.bottomFiveScroll.setBounds(500, 200, 150, 150);
        this.getContentPane().add( bottomFiveScroll);
       
        this.countWordsButton.setBounds(400,80,100,30);
        this.getContentPane().add( countWordsButton);
        this.countWordsButton.addActionListener( this);
        
        this.fileButton.setBounds(250, 80, 100, 30);
        this.getContentPane().add( fileButton);
        this.fileButton.addActionListener( this);
    }
    
    /**
     * This method dictates what happens when a user hits the 'Count it!' button
     * Methods within Functions.java are called
     */
    private void theCountitButtonHasBeenPushed() throws IOException {
         String text = fileName.getText();
         
         Functions myFunction = new Functions();
         
         HashMap tempHash = new HashMap();
         tempHash = myFunction.addWords(text); //Set text file contents to a local hashmap
         
         //Formatting hashmap to print neatly to TextArea answer
         //Then use stringbuilder to format nicely
         StringBuilder str = new StringBuilder();
         ArrayList<String> printedHashMap = new ArrayList<String>(tempHash.keySet());
         Collections.sort(printedHashMap);

         for (String key : printedHashMap) {
             str.append(key);
             str.append(" : ");
             str.append(myFunction.addWords(text).get(key));
             str.append("\n");
         }
         
         //Called topFive method from Functions.java to take top 5 words
         //Then use stringbuilder to format nicely
         StringBuilder str2 = new StringBuilder();
         for (int i = 1; i <= 5; i++) {
            str2.append(myFunction.topFive(tempHash).get(myFunction.topFive(tempHash).size() - i));
            str2.append("\n");
        }
         
         //Called bottomFive method from Functions.java to take bottom 5 words
         //Then use stringbuilder to format nicely
         
         StringBuilder str3 = new StringBuilder();
         for (int i = 1; i <= 5; i++) {
            str3.append(myFunction.bottomFive(tempHash).get(i));
            str3.append("\n");
        }
         
         //Print our new stringbuilders to our GUI text areas
         
         fullAnswer.setText(str.toString());
         topFiveAnswer.setText(str2.toString());
         bottomFiveAnswer.setText(str3.toString());

         
         
       
    }
    
    /**
     * This method details what happens when 'Pick a File' button is hit
     * Allows users to selected a file from the given computer
     */
    private void theFileButtonHasBeenPushed() {
        System.out.println("Hit the file button");
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter( new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
        System.out.println("I created the file chooser");
        int chooserSuccess = chooser.showOpenDialog( null);
        System.out.println("I opened the file chooser, it returned " + chooserSuccess);
        
        if( chooserSuccess == JFileChooser.APPROVE_OPTION) {
            File chosenFile = chooser.getSelectedFile(); //Takes in a file type
            System.out.println("You chose the file " + chosenFile.getAbsolutePath());
            System.out.println("You chose the file " + chosenFile.getName());
            this.fileName.setText(chosenFile.getName());
        }
        else {
            System.out.println("You hit cancel");
        }
        
    }
    
        
       
    /**
     * Callback Response to user pushing file or count it button
     * @param ae  The action event, including the button command
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
         System.out.println("The action event is " + ae);
         if(ae.getActionCommand().equals("Count it!")) {
             try {
                 this.theCountitButtonHasBeenPushed();
             } catch (IOException ex) {
                 Logger.getLogger(GuiWindow.class.getName()).log(Level.SEVERE, null, ex);
             }
         } 
         else if (ae.getActionCommand().equals("Pick a File!")) {
             this.theFileButtonHasBeenPushed();
         }
         
    }
    
}
