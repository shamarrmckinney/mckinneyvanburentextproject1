
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class hold all functions to process a text file Outputs a hash map of
 * words from text file and their use frequency Also outputs top five and bottom
 * five used words Prints to console and to an external text file
 *
 * @author Shamarr McKinney-VanBuren
 */
public class Functions {          

    /**
     * This method statically adds words from a text file to a hash map Then
     * calculates their use frequency, sorts, and outputs to console & text
     * @param textInput inputs a given text file to process
     * @return a hash map of words from a text file and their use frequency
     * @throws java.io.IOException
     */
    public static HashMap addWords(String textInput) throws IOException { 

        // Creating Buffer to read into a file
        BufferedReader buff = new BufferedReader(new FileReader(textInput)); 
        //Creating Hashmap object to store our text data!
        HashMap<String, Integer> myHashMap = new HashMap<String, Integer>();
        String words;

        //Loop to Read through contents of text file, strip text of punctuation
        //And capitalization. Then, split data to isolate each word in an array
        while ((words = buff.readLine()) != null) {
            words = words.replaceAll("\\W", " ").replaceAll("  ", " ").toLowerCase();
            String wordArray[] = words.split(" ");

            //Loop through the text array and add to hash map, Initialize value to 1
            //If given key is already in hash map, increment value by 1
            for (int i = 0; i <= wordArray.length - 1; i++) {

                if (!myHashMap.containsKey(wordArray[i])) {
                    myHashMap.put(wordArray[i], 1);
                } else {
                    myHashMap.put(wordArray[i], myHashMap.get(wordArray[i]) + 1);
                }
            }
        }

        //Alphabetically sorting & printing hashmap to output file
        //And to console
        File file = new File("output.txt");

        //If file doesn't exist, create new file
        if (!file.exists()) {
            file.createNewFile();
        }

        //Creating File and Buffer writer to write to new output file
        FileWriter fw = new FileWriter(file);
        BufferedWriter buffw = new BufferedWriter(fw);

        //Creating ArrayList to easily sort hashtable alpphabetically, then sorting
        ArrayList<String> sortedKeys = new ArrayList<String>(myHashMap.keySet());
        Collections.sort(sortedKeys);

        System.out.println("===HASHMAP===");

        for (String key : sortedKeys) {
            buffw.write(key + ':' + myHashMap.get(key).toString() + '\n');
            System.out.println(key + ':' + myHashMap.get(key).toString());
        }

        buff.close(); //Closing buffer-reader
        buffw.close(); //closing buffer-writer

        //Now, call the below functions to find top and bottom 5 used words
        topFive(myHashMap);
        bottomFive(myHashMap);

        return myHashMap;

    }

    /**
     * This method outputs top 5 used words in a given text file
     *
     * @param myHashMap a hash map of data to sort
     * @return myList a list with sorted hashmap data
     */
    public static List<Map.Entry<String, Integer>> topFive(HashMap myHashMap) throws IOException {

        List<Map.Entry<String, Integer>> myList
                = new LinkedList<Map.Entry<String, Integer>>(myHashMap.entrySet());

        // Use collections and comparator to sort the new linked list
        Collections.sort(myList, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                    Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        //Print out hashmap info to console and output file!
        //Creating File and Buffer writer to write to new output file
        File file = new File("top5.txt");

        //If file doesn't exist, create new file
        if (!file.exists()) {
            file.createNewFile();
        }

        //Creating File and Buffer writer to write to new output file
        FileWriter fw1 = new FileWriter(file);
        BufferedWriter buffw1 = new BufferedWriter(fw1);

        System.out.println("===TOP 5 USED WORDS===");

        //Print out last 5 values, also the top 5 values 
        for (int i = 1; i <= 5; i++) {
            System.out.println(myList.get(myList.size() - i));
            buffw1.write(myList.get(myList.size() - i).toString() + '\n');
        }

        buffw1.close(); //Closing another buffer-writer
        
        return myList;

    }

    /**
     * This method output bottom 5 used words in a given text file
     *
     * @param myHashMap a hash map of data to sort
     * @return myList a list of sorted hashmap data
     */
    public static List<Map.Entry<String, Integer>> bottomFive(HashMap myHashMap) throws IOException {

        List<Map.Entry<String, Integer>> myList
                = new LinkedList<Map.Entry<String, Integer>>(myHashMap.entrySet());

        // Use collections and comparator to sort the new linked list
        Collections.sort(myList, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                    Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        //Print out hashmap info to console and output file!
        //Creating File and Buffer writer to write to new output file
        File file = new File("bottom5.txt");

        //If file doesn't exist, create new file
        if (!file.exists()) {
            file.createNewFile();
        }

        //Creating File and Buffer writer to write to new output file
        FileWriter fw2 = new FileWriter(file);
        BufferedWriter buffw2 = new BufferedWriter(fw2);

        System.out.println("===BOTTOM 5 USED WORDS===");

        //Print out beginning 5 values, also the last 5 values 
        for (int i = 1; i <= 5; i++) {
            System.out.println(myList.get(i));
            buffw2.write(myList.get(i).toString() + '\n');
        }

        buffw2.close(); //Closing another buffer-writer

        return myList;
    }

}
