

/**
 * Runs/'Drives' the GUI window & program as created in GuiWindow.java
 * @author Shamarr McKinney-VanBuren
 */
public class GuiDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Starting GUI window
        
        GuiWindow myWindow = new GuiWindow();
        myWindow.setVisible( true);
    }
    
}
