Readme File for my Java Text Reading Project 1
Last Updated: Sunday, 2/16/2020 
Author: Shamarr McKinney-VanBuren 
Latest version available: https://bitbucket.org/shamarrmckinney/mckinneyvanburentextproject1/src

== Overview ==

This program is designed to process text from an external .txt file and return words and the amount of time those words appear in the text into an external file.

Also returns the top 5 used words and the bottom 5 used words  

Source code found in src folder in bit bucket repository. 

To compile via IDE, run source code in a Java supported IDE, with an external text file to process. Make sure previously commented out portion of Driver.java is uncommented (also comment out the previously uncommented code), then run Driver.java

To use GUI window, run GuiDriver.java versus the normal Driver.java. 

Also can be run in a command-line terminal using javac commands (run Driver.java for traditional program (making sure the string args driver code is uncommented), run GuiDriver.java for GUI window to open)

To run program successfully, it's important to have an input text file for the program to read.

Said text file NEEDS to be in a directory that the program can find. Recommended to put the text file and the program in the same directory path.  


=======================================================================