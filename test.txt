The term “art” might put some readers off. If cinema originated as a mass medium,
should we even use the word? Are Hollywood directors “artists”? Some people
would say that the blockbusters playing at the multiplex are merely “entertainment,” 
but films for a narrower public—perhaps independent films, or foreign language fare, 
or experimental works—are true art.